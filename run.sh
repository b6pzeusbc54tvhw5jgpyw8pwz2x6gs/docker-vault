mkdir file
docker stop vault
docker rm vault
docker run \
	-d \
	--name vault \
	--cap-add IPC_LOCK \
	-v $PWD/config:/vault/config \
	-v $PWD/file:/vault/file \
	--expose 8200 \
	--env VIRTUAL_HOST=vault.alfreduc.com \
	--env VIRTUAL_PORT=8200 \
	vault:0.6.2 server
